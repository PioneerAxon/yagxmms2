#include "connection_manager.h"

bool sync_play (xmms_connection* connection)
{
	xmmsc_result_t* result;
	xmmsv_t* result_value;

	result = xmmsc_playback_start (connection->connect);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);
	return true;
}


bool sync_pause (xmms_connection* connection)
{
	xmmsc_result_t* result;
	xmmsv_t* result_value;

	result = xmmsc_playback_pause (connection->connect);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);
	return true;
}

bool sync_stop (xmms_connection* connection)
{
	xmmsc_result_t* result;
	xmmsv_t* result_value;

	result = xmmsc_playback_stop (connection->connect);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);
	return true;
}


bool sync_next (xmms_connection* connection)
{
	xmmsc_result_t* result;
	xmmsv_t* result_value;

	result = xmmsc_playlist_set_next_rel (connection->connect, 1);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);

	result = xmmsc_playback_tickle (connection->connect);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);
	return true;
}


bool sync_prev (xmms_connection* connection)
{
	xmmsc_result_t* result;
	xmmsv_t* result_value;

	result = xmmsc_playlist_set_next_rel (connection->connect, -1);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);

	result = xmmsc_playback_tickle (connection->connect);
	xmmsc_result_wait (result);
	result_value = xmmsc_result_get_value (result);
	if (xmmsv_is_error (result_value))
	{
		xmmsc_result_unref (result);
		return false;
	}
	xmmsc_result_unref (result);
	return true;
}
