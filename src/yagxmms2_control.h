/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#ifndef __YAGXMMS2_CONTROL_H__
#define __YAGXMMS2_CONTROL_H__

#include <xmmsclient/xmmsclient.h>
#include <xmmsclient/xmmsclient-glib.h>

void xmms_pause (void (*callback) (void*), void* data);

void xmms_play (void (*callback) (void*), void* data);

void xmms_next (void (*callback) (void*), void* data);

void xmms_prev (void (*callback) (void*), void* data);

void xmms_stop (void (*callback) (void*), void* data);

#endif
