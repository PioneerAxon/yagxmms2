/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#include <gtk/gtk.h>
#include <stdio.h>

#include "yagxmms2_app.h"
#include "yagxmms2_window.h"

struct yagxmms2_app_window
{
	GtkApplicationWindow parent;
};

struct yagxmms2_app_window_class
{
	GtkApplicationWindowClass parent_class;
};

G_DEFINE_TYPE (YAGxmms2AppWindow, yagxmms2_app_window, GTK_TYPE_APPLICATION_WINDOW);

static void
yagxmms2_app_window_init (YAGxmms2AppWindow* win)
{
	gtk_widget_init_template (GTK_WIDGET (win));
}

static void
yagxmms2_app_window_class_init (YAGxmms2AppWindowClass* class)
{
	gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/org/xmms2/yagxmms2/window.ui");
}

YAGxmms2AppWindow*
yagxmms2_app_window_new (YAGxmms2App* app)
{
	return g_object_new (YAGXMMS2_APP_WINDOW_TYPE, "application", app, NULL);
}

void
yagxmms2_app_window_open (YAGxmms2AppWindow* win, GFile* file)
{
}
