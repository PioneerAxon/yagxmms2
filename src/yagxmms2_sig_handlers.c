/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#include <gtk/gtk.h>

#include "yagxmms2_control.h"


void
yagxmms2_clicked_previous (GtkButton* button, gpointer user)
{
	xmms_prev (NULL, NULL);
}

void
yagxmms2_clicked_play (GtkButton* button, gpointer user)
{
	xmms_play (NULL, NULL);
}

void
yagxmms2_clicked_pause (GtkButton* button, gpointer user)
{
	xmms_pause (NULL, NULL);
}

void
yagxmms2_clicked_stop (GtkButton* button, gpointer user)
{
	xmms_stop (NULL, NULL);
}

void
yagxmms2_clicked_next (GtkButton* button, gpointer user)
{
	xmms_next (NULL, NULL);
}

void
yagxmms2_application_shutdown (GApplication* application, gpointer userdata)
{
	xmms_connection_destroy ();
}
