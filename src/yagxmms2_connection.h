/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#ifndef __YAGXMMS2_CONNECTION_H__
#define __YAGXMMS2_CONNECTION_H__

#include <stdlib.h>
#include <xmmsclient/xmmsclient.h>
#include <glib.h>

#define _CLIENT_NAME "YAGxmms2"

typedef struct
{
	xmmsc_connection_t *connect;
} xmms_connection;


gboolean xmms_connection_new ();

void xmms_connection_destroy ();

xmmsc_connection_t* xmms_connection_get ();

#endif
