#include <stdlib.h>

#include "connection_manager.h"

bool sync_play (xmms_connection*);

bool sync_pause (xmms_connection*);

bool sync_stop (xmms_connection*);

bool sync_next (xmms_connection*);

bool sync_prev (xmms_connection*);
