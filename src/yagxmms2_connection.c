/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#include "yagxmms2_connection.h"

xmms_connection* _yagxmms2_xmms_connection = NULL;

gboolean
xmms_connection_new ()
{
	xmms_connection* connection;
	connection = (xmms_connection*) malloc (sizeof (xmms_connection));
	if (connection == NULL)
		return false;
	connection->connect = xmmsc_init (_CLIENT_NAME);
	if (connection->connect == NULL)
		return false;
	if (!xmmsc_connect (connection->connect, getenv ("XMMS_PATH")))
		return false;
	_yagxmms2_xmms_connection = connection;
	return true;
}

void
xmms_connection_destroy ()
{
	if (_yagxmms2_xmms_connection && _yagxmms2_xmms_connection->connect)
		xmmsc_unref (_yagxmms2_xmms_connection->connect);
	if (_yagxmms2_xmms_connection)
		free (_yagxmms2_xmms_connection);
	_yagxmms2_xmms_connection = NULL;
}

xmmsc_connection_t*
xmms_connection_get ()
{
	return _yagxmms2_xmms_connection->connect;
}
