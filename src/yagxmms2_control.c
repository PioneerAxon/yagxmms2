/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#include "yagxmms2_control.h"
#include "yagxmms2_connection.h"

#define CONNECTION (xmms_connection_get ())

typedef struct
user_data
{
	void (*callback) (void*);
	void* data;
} UserData;

static int
xmms_common_callback (xmmsv_t* value, void* data)
{
	if (xmmsv_is_error (value))
		printf ("Error occured!\n");
	UserData* udata = (UserData*) data;
	if (udata && udata->callback)
		(*(udata->callback)) (udata->data);
	if (udata)
		free (udata);
	return TRUE;
}

void
xmms_pause (void (*callback) (void*), void* data)
{
	UserData* udata = (UserData*) malloc (sizeof (UserData));
	if (!udata)
		return;
	udata->callback = callback;
	udata->data = data;
	XMMS_CALLBACK_SET (CONNECTION, xmmsc_playback_pause, xmms_common_callback, udata);
}

void
xmms_play (void (*callback) (void*), void* data)
{
	UserData* udata = (UserData*) malloc (sizeof (UserData));
	if (!udata)
		return;
	udata->callback = callback;
	udata->data = data;
	XMMS_CALLBACK_SET (CONNECTION, xmmsc_playback_start, xmms_common_callback, udata);
}

void
xmms_next (void (*callback) (void*), void* data)
{
	UserData* udata = (UserData*) malloc (sizeof (UserData));
	if (!udata)
		return;
	udata->callback = callback;
	udata->data = data;
	xmmsc_result_unref (xmmsc_playlist_set_next_rel (CONNECTION, 1));
	XMMS_CALLBACK_SET (CONNECTION, xmmsc_playback_tickle, xmms_common_callback, udata);
}

void
xmms_prev (void (*callback) (void*), void* data)
{
	UserData* udata = (UserData*) malloc (sizeof (UserData));
	if (!udata)
		return;
	udata->callback = callback;
	udata->data = data;
	xmmsc_result_unref (xmmsc_playlist_set_next_rel (CONNECTION, -1));
	XMMS_CALLBACK_SET (CONNECTION, xmmsc_playback_tickle, xmms_common_callback, udata);
}

void
xmms_stop (void (*callback) (void*), void* data)
{
	UserData* udata = (UserData*) malloc (sizeof (UserData));
	if (!udata)
		return;
	udata->callback = callback;
	udata->data = data;
	XMMS_CALLBACK_SET (CONNECTION, xmmsc_playback_stop, xmms_common_callback, udata);
}
