/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#ifndef __YAGXMMS2_WINDOW_H__
#define __YAGXMMS2_WINDOW_H__

#include <gtk/gtk.h>

#include "yagxmms2_app.h"

#define YAGXMMS2_APP_WINDOW_TYPE (yagxmms2_app_window_get_type ())
#define YAGXMMS2_APP_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), YAGXMMS2_APP_WINDOW_TYPE, YAGxmms2AppWindow))

typedef struct yagxmms2_app_window YAGxmms2AppWindow;
typedef struct yagxmms2_app_window_class YAGxmms2AppWindowClass;

GType			yagxmms2_app_window_get_type	(void);
YAGxmms2AppWindow*	yagxmms2_app_window_new		(YAGxmms2App* app);
void			yagxmms2_app_window_open	(YAGxmms2AppWindow* win, GFile* file);

#endif
