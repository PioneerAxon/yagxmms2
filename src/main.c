/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

#include <yagxmms2_app.h>

int main (int argc, char* argv [])
{
	return g_application_run (G_APPLICATION (yagxmms2_app_new ()), argc, argv);
}
