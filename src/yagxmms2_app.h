/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#ifndef __YAGXMMS2_APP_H__
#define __YAGXMMS2_APP_H__

#include <gtk/gtk.h>

#define YAGXMMS2_APP_TYPE (yagxmms2_app_get_type ())
#define YAGXMMS2_APP(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), YAGXMMS2_APP_TYPE, YAGxmms2App))

typedef struct yagxmms2_app YAGxmms2App;
typedef struct yagxmms2_app_class YAGxmms2AppClass;


GType		yagxmms2_app_get_type	(void);
YAGxmms2App*	yagxmms2_app_new	(void);


#endif
