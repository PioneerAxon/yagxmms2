/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#ifndef __YAGXMMS2_SIG_HANDLERS_H__
#define __YAGXMMS2_SIG_HANDLERS_H__

#include <gtk/gtk.h>

void yagxmms2_application_shutdown (GApplication* application, gpointer userdata);

#endif
