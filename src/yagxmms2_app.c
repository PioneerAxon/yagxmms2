/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See <http://www.gnu.org/licenses/> for the full text of the license.
 *
 */

#include <gtk/gtk.h>

#include "yagxmms2_app.h"
#include "yagxmms2_window.h"
#include "yagxmms2_sig_handlers.h"
#include "yagxmms2_connection.h"
#include "config.h"

struct yagxmms2_app
{
	GtkApplication parent;
};

struct yagxmms2_app_class
{
	GtkApplicationClass parent_class;
};

G_DEFINE_TYPE (YAGxmms2App, yagxmms2_app, GTK_TYPE_APPLICATION);

static char* authors[] =
{
	"Arth Patel (PioneerAxon)",
	NULL
};

static void
about_activated (GSimpleAction* action, GVariant* parameter, gpointer app)
{
	gtk_show_about_dialog (NULL, "authors", authors, "license-type", GTK_LICENSE_GPL_3_0, "program-name", PACKAGE_NAME, "version", VERSION, "website", "https://bitbucket.org/PioneerAxon/yagxmms2", "copyright", "Arth Patel (PioneerAxon) <arth.svnit@gmail.com>", "logo-icon-name", "yagxmms2", NULL);
}

static void
quit_activated (GSimpleAction* action, GVariant* parameter, gpointer app)
{
	g_application_quit (G_APPLICATION (app));
}

static GActionEntry app_entries[] =
{
	{ "about", about_activated, NULL, NULL, NULL },
	{ "quit", quit_activated, NULL, NULL, NULL }
};

static void
yagxmms2_app_startup (GApplication *app)
{
	GtkBuilder *builder;
	GMenuModel *app_menu;

	G_APPLICATION_CLASS (yagxmms2_app_parent_class)->startup (app);

	g_action_map_add_action_entries (G_ACTION_MAP (app), app_entries, G_N_ELEMENTS (app_entries), app);

	builder = gtk_builder_new_from_resource ("/org/xmms2/yagxmms2/app-menu.ui");
	app_menu = G_MENU_MODEL (gtk_builder_get_object (builder, "appmenu"));
	gtk_application_set_app_menu (GTK_APPLICATION (app), app_menu);
	g_object_unref (builder);
}

static void
yagxmms2_app_init (YAGxmms2App* app)
{
}

static void
yagxmms2_app_activate (GApplication* app)
{
	YAGxmms2AppWindow* win;

	g_signal_connect (app, "shutdown", G_CALLBACK (yagxmms2_application_shutdown), NULL);

	win = yagxmms2_app_window_new (YAGXMMS2_APP (app));
	gtk_window_set_default_icon_name ("yagxmms2");
	gtk_window_present (GTK_WINDOW (win));

	//TODO: Replace quit with something more informative (e.g. popup).
	if (!xmms_connection_new ())
		g_application_quit (G_APPLICATION (app));
	xmmsc_mainloop_gmain_init (xmms_connection_get ());
}

static void
yagxmms2_app_open (GApplication* app, GFile **files, gint n_files, const gchar* hint)
{
	GList* windows;
	YAGxmms2AppWindow* win;
	int i;
	windows = gtk_application_get_windows (GTK_APPLICATION (app));
	if (windows)
		win = YAGXMMS2_APP_WINDOW (windows->data);
	else
		win = yagxmms2_app_window_new (YAGXMMS2_APP (app));

	for (i = 0; i < n_files; i++)
	{
		yagxmms2_app_window_open (win, files [i]);
	}

	gtk_window_present (GTK_WINDOW (win));
}

static void
yagxmms2_app_class_init (YAGxmms2AppClass *class)
{
	G_APPLICATION_CLASS (class)->activate = yagxmms2_app_activate;
	G_APPLICATION_CLASS (class)->open = yagxmms2_app_open;
	G_APPLICATION_CLASS (class)->startup = yagxmms2_app_startup;
}

YAGxmms2App*
yagxmms2_app_new (void)
{
	return g_object_new (YAGXMMS2_APP_TYPE, "application-id", "org.xmms2.yagxmms2", "flags", G_APPLICATION_HANDLES_OPEN, NULL);
}
