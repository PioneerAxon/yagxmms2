#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include <connection_manager.h>
#include <sync_control.h>

static GtkBuilder* builder;
static xmms_connection* connection;

void on_yagxmms2_destroy (GtkWidget* object, gpointer user_data)
{
	gtk_main_quit ();
}

void on_play_clicked (GtkToolButton* button, gpointer user_data)
{
	sync_play (connection);
}

void on_pause_clicked (GtkToolButton* button, gpointer user_data)
{
	sync_pause (connection);
}

void on_help_about_menu_activate (GtkMenuItem* menu, gpointer user_data)
{
	GtkWidget* about;
	about = GTK_WIDGET (gtk_builder_get_object (builder, "about_dialog_box"));
	gtk_dialog_run (GTK_DIALOG (about));
	gtk_widget_hide (about);
}

void on_previous_clicked (GtkToolButton* button, gpointer user_data)
{
	sync_prev (connection);
}

void on_next_clicked (GtkToolButton* button, gpointer user_data)
{
	sync_next (connection);
}

void on_stop_clicked (GtkToolButton* button, gpointer user_data)
{
	sync_stop (connection);
}

int main (int argc, char *argv[])
{
	GtkWidget* window;
	gtk_init (&argc, &argv);
	builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (builder, "../data/main.glade", NULL))
	{
		fprintf (stderr, "Can not load file main.glade.\nMake sure it exists.\n");
		return EXIT_FAILURE;
	}
	window = GTK_WIDGET (gtk_builder_get_object (builder, "yagxmms2"));
	gtk_builder_connect_signals (builder, NULL);
	gtk_widget_show (window);
	connection = init_connection ();
	gtk_main ();
	exit_connection (connection);
	g_object_unref (builder);
	return EXIT_SUCCESS;
}
